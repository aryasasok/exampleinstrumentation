package com.example.exampleinstrumentation;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.junit.Assert.*;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static java.lang.Thread.sleep;
import android.support.test.rule.ActivityTestRule;

/**
 * Instrumented test, which will execute on an Android device.
 *;
 * See [testing documentation](http://d.android.com/tools/testing).
 */

public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testGoingToNextPage() throws Exception {
        // onView(withId(R.id.______))

        // get the button and click it
        onView(withId(R.id.btButton)).perform(click());

        sleep(1000);        // i don't know if sleep actually works!

        // on next page, check that there is a label called "text_header"
        onView(withId(R.id.tvHeader)).check(matches(isDisplayed()));

        sleep(3000);

        // go back to previous page
        //  -- find reply button and click on it?
        onView(withId(R.id.btButton)).perform(click());

        sleep(1000);
    }

    // TC2:  Test then when you type nonsense into box, text appears on page 2
    @Test
    public void testInputBox() throws Exception {
        // 1. find the text box
        // 2. type some nonsese (sendKeys() in selenium)
        onView(withId(R.id.editText_main)).perform(typeText("HELLO JENELLE!"));

        // 3. click on SEND button (find the button + click)
        onView(withId(R.id.btButton)).perform(click());

        // 4. make sure text appears

        sleep(3000);

        // matches(isDisplayed()) checks if LABEL is SHOWN (appears on screen)
        onView(withId(R.id.tvMesage)).check(matches(isDisplayed()));

        // matches(withText("....")) checks the WORDS inside the label
        String expectedOutput = "HELLO JENELLE!";
        onView(withId(R.id.tvMesage)).check(matches(withText(expectedOutput)));

        sleep(1000);

    }



}
